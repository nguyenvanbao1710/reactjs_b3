import React, { Component } from "react";
import ProductItem from "./ProductItem";
export default class ProductList extends Component {
  render() {
    const { mangDt, themGioHang } = this.props;

    return (
      <div className="container">
        <div className="row">
          {mangDt.map((item, index) => {
            return (
              <div className="col-4" key={index}>
                <ProductItem themGioHang={themGioHang} sanPham={item} />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
