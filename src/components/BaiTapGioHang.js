import React, { Component } from "react";
import { dataPhoneShop } from "./dataPhoneShop";
import ModalGioHang from "./ModalGioHang";
import ProductList from "./Productlist";

export default class BaiTapGioHang extends Component {
  state = {
    gioHang: [],
  };

  handleThemGioHang = (sanPham) => {
    let sanPhamGioHang = {
      maSP: sanPham.maSP,
      tenSP: sanPham.tenSP,
      giaBan: sanPham.giaBan,
      hinhAnh: sanPham.hinhAnh,
      soLuong: 1,
    };
    //kiem tra sp trong gio hang
    let updateCart = [...this.state.gioHang];
    let index = updateCart.findIndex((sp) => sp.maSP === sanPhamGioHang.maSP);
    index !== -1
      ? (updateCart[index].soLuong += 1)
      : updateCart.push(sanPhamGioHang);
    this.setState({
      gioHang: updateCart,
    });
  };

  handleXoaGioHang = (maSP) => {
    let updateCart = this.state.gioHang.filter((sp) => sp.maSP !== maSP);
    this.setState({
      gioHang: updateCart,
    });
  };

  handleTangGiamSoLuong = (maSP, tangGiam) => {
    let updateCart = [...this.state.gioHang];
    let index = updateCart.findIndex((sp) => sp.maSP === maSP);
    if (tangGiam) {
      updateCart[index].soLuong += 1;
    } else {
      if (updateCart[index].soLuong > 1) {
        updateCart[index].soLuong -= 1;
      }
    }
    this.setState({
      gioHang: updateCart,
    });
  };

  render() {
    let tongSoLuong = this.state.gioHang.reduce((tsl, spGH, index) => {
      return (tsl += spGH.soLuong);
    }, 0);
    return (
      <div className="container">
        <h1>Bài tập giỏ hàng</h1>
        <ModalGioHang
          xoaGioHang={this.handleXoaGioHang}
          gioHang={this.state.gioHang}
          tangGiamSoLuong={this.handleTangGiamSoLuong}
        />
        <div className="text-right">
          <span
            className="text-danger"
            data-toggle="modal"
            data-target="#modelId"
            style={{ cursor: "pointer" }}
          >
            <i className="fa fa-shopping-cart"></i> ({tongSoLuong})
          </span>
        </div>
        <ProductList
          themGioHang={this.handleThemGioHang}
          mangDt={dataPhoneShop}
        />
      </div>
    );
  }
}
