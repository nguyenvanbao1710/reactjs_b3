import React, { Component } from "react";

export default class ModalGioHang extends Component {
  render() {
    const { gioHang, xoaGioHang, tangGiamSoLuong } = this.props;
    return (
      <div>
        <div
          className="modal fade"
          id="modelId"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div
            className="modal-dialog"
            role="document"
            style={{ maxWidth: "900px" }}
          >
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Giỏ hàng</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table className="table">
                  <thead>
                    <tr>
                      <td>Mã sản phẩm</td>
                      <td>Hình ảnh</td>
                      <td>Tên sản phẩm</td>
                      <td>Đơn giá</td>
                      <td>Số lượng</td>
                      <td>Thành tiền</td>
                    </tr>
                  </thead>
                  <tbody>
                    {gioHang.map((spGH, index) => {
                      return (
                        <tr key={index}>
                          <td>{spGH.maSP}</td>
                          <td>
                            <img
                              src={spGH.hinhAnh}
                              width={50}
                              height={50}
                              alt=""
                            />
                          </td>
                          <td>{spGH.tenSP}</td>
                          <td>{spGH.giaBan}</td>
                          <td>
                            <i
                              onClick={() => {
                                tangGiamSoLuong(spGH.maSP, true);
                              }}
                              className="fa fa-plus mr-2 text-warning"
                            ></i>

                            {spGH.soLuong}

                            <i
                              onClick={() => {
                                tangGiamSoLuong(spGH.maSP, false);
                              }}
                              className="fa fa-minus ml-2 text-warning"
                            ></i>
                          </td>
                          <td>{spGH.soLuong * spGH.giaBan}</td>
                          <td>
                            <i
                              className="fa fa-trash-alt text-danger"
                              onClick={() => {
                                xoaGioHang(spGH.maSP);
                              }}
                            ></i>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
