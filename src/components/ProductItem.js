import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    const { sanPham, themGioHang } = this.props;
    return (
      <div>
        <div className="card bg-light">
          <img
            className="card-img-top"
            src={sanPham.hinhAnh}
            alt={sanPham.hinhAnh}
            height={350}
          />
          <div className="card-body">
            <h4 className="card-title">{sanPham.tenSP}</h4>
            <button className="btn btn-success">Xem chi tiết</button>
            <button
              onClick={() => {
                themGioHang(sanPham);
              }}
              className="btn btn-danger ml-4"
            >
              Thêm vào giỏ hàng
            </button>
          </div>
        </div>
      </div>
    );
  }
}
